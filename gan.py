import numpy as np

from utils import *

class GAN:
    def __init__(self,
                 epochs,
                 k_steps=1,
                 batch_size=16):
        self.epochs = epochs
        self.k_steps = k_steps
        self.batch_size = batch_size
        self.learning_rate = 0.001
        self.dtype = float
        self.alpha = 0.2
        self.rate = 0.3
        # generator weight inits
        self.g_z_dim = 100
        self.g_h0_dim = 256
        self.g_h1_dim = 512
        self.g_h2_dim = 1024
        self.g_out_dim = 784
        self.g_params = {}
        self.g_params['w0'], self.g_params['b0'] = weights_bias_init(
            self.g_z_dim, self.g_h0_dim, self.dtype)
        self.g_params['w1'], self.g_params['b1'] = weights_bias_init(
            self.g_h0_dim, self.g_h1_dim, self.dtype)
        self.g_params['w2'], self.g_params['b2'] = weights_bias_init(
            self.g_h1_dim, self.g_h2_dim, self.dtype)
        self.g_params['w3'], self.g_params['b3'] = weights_bias_init(
            self.g_h2_dim, self.g_out_dim, self.dtype)

        # discriminator weight inits
        self.d_x_dim = 784
        self.d_h0_dim = 1024
        self.d_h1_dim = 512
        self.d_h2_dim = 256
        self.d_out_dim = 1
        self.d_params = {}
        self.d_params['w0'], self.d_params['b0'] = weights_bias_init(
            self.d_x_dim, self.d_h0_dim, self.dtype)
        self.d_params['w1'], self.d_params['b1'] = weights_bias_init(
            self.d_h0_dim, self.d_h1_dim, self.dtype)
        self.d_params['w2'], self.d_params['b2']= weights_bias_init(
            self.d_h1_dim, self.d_h2_dim, self.dtype)
        self.d_params['w3'], self.d_params['b3'] = weights_bias_init(
            self.d_h2_dim, self.d_out_dim, self.dtype)

    def train(self, x):
        num_batches = x.shape[0]//self.batch_size
        print('Starting traing....')
        for i in range(self.epochs):
            for j in range(num_batches):
                x_batch = x[j * self.batch_size:j * self.batch_size + self.batch_size]
                assert x_batch.shape == (self.batch_size, x.shape[1])
                for k in range(self.k_steps):
                    pass
                    z = np.random.uniform(size=(self.batch_size, 100))
                    # forward pass
                    gout, gcache = gan.generator(z)

                    dout, dcache = gan.discriminator(x_batch)
                    dout_synthetic, dcache_synthetic = gan.discriminator(gout)

                    _, d_loss = gan.backprop_discriminator(dout, dout_synthetic, dcache, dcache_synthetic)
                
                z = np.random.uniform(size=(self.batch_size, 100))
                # forward pass
                gout, gcache = gan.generator(z)

                dout, dcache = gan.discriminator(x_batch)
                dout_synthetic, dcache_synthetic = gan.discriminator(gout)

                dx, _ = gan.backprop_discriminator(dout, dout_synthetic, dcache, dcache_synthetic)
                g_loss = gan.backprop_generator(dout_synthetic, gcache, dx)
            print('Epoch: {}, Discriminator Loss: {}, Generator Loss: {}'.format(
                i, np.mean(d_loss), np.mean(g_loss)
            ))
        print('Training ends!')
    
    def generator(self, z):
        h0 = np.dot(z, self.g_params['w0']) + self.g_params['b0']
        lrelu0 = lrelu(h0, self.alpha)

        h1 = np.dot(lrelu0, self.g_params['w1']) + self.g_params['b1']
        lrelu1 = lrelu(h1, self.alpha)

        h2 = np.dot(lrelu1, self.g_params['w2']) + self.g_params['b2']
        lrelu2 = lrelu(h2, self.alpha)

        h3 = np.dot(lrelu2, self.g_params['w3']) + self.g_params['b3']
        tanh_out = tanh(h3)
        cache = (z, h0, lrelu0, h1, lrelu1, h2, lrelu2, h3)
        return tanh_out, cache

    def discriminator(self, x):
        h0 = np.dot(x, self.d_params['w0']) + self.d_params['b0']
        lrelu0 = lrelu(h0, self.alpha)
        drop0 = dropout(lrelu0, self.rate)

        h1 = np.dot(drop0, self.d_params['w1']) + self.d_params['b1']
        lrelu1 = lrelu(h1, self.alpha)
        drop1 = dropout(lrelu1, self.rate)

        h2 = np.dot(drop1, self.d_params['w2']) + self.d_params['b2']
        lrelu2 = lrelu(h2, self.alpha)
        drop2 = dropout(lrelu2, self.rate)

        h3 = np.dot(drop2, self.d_params['w3']) + self.d_params['b3']
        sigm = sigmoid(h3)

        cache = (x, h0, lrelu0, drop0, h1, lrelu1, drop1 , h2, lrelu2, drop2, h3)
        return sigm, cache

    def backprop_discriminator(self, sigm, sigm_synthetic, d_out_cache, d_g_out_cache):
        # Discriminator loss = -np.mean(log(D(x)) + log(1-D(G(z))))
        # first term of the loss -log(D(x))
        loss_1 = -1 * np.log(sigm + 10e-4)/self.batch_size
        grads_1, _ = self._backprop_d(loss_1, sigm, d_out_cache)
        # second term of the loss -log(1-D(G(z)))
        # print('sigm_synthetic: ', sigm_synthetic)
        loss_2 = -1 * np.log(1 - sigm_synthetic + 10e-4)/self.batch_size
        grads_2, dx = self._backprop_d(loss_2, sigm_synthetic, d_g_out_cache)
        loss = loss_1 + loss_2
        # updates
        for key, _ in grads_1.items():
            grads_1[key] += grads_2[key]
            self.d_params[key] -= self.learning_rate * grads_1[key]
        # done
        return dx, loss

    def backprop_generator(self, sigm_synthetic, cache, d_x):
        loss = np.log(1 - sigm_synthetic + 10e-4)/self.batch_size
        grads, _ = self._backprop_g(loss, cache, d_x)
        for key, _ in grads.items():
            self.g_params[key] -= self.learning_rate * grads[key]
        # done
        return loss

    def _backprop_d(self, loss, sigm, cache):
        grads = {}
        d_sigm = 1/(loss + 10e-4)
        # gradient on D(x)
        d_h3 = d_sigm * sigmoid(cache[10], derivative=True)
        # affine layer       
        dd_drop2 = np.dot(d_h3, self.d_params['w3'].T)
        grads['w3'] = np.dot(cache[9].T, d_h3)
        grads['b3'] = np.sum(d_h3, axis=0)
        # backprop drop2
        d_lrelu2 = dd_drop2 * dropout(cache[8], self.rate, derivative=True)
        # gradient on h2
        d_h2 = d_lrelu2 * lrelu(cache[7], self.alpha, derivative=True)
        # affine layer
        dd_drop1 = np.dot(d_h2, self.d_params['w2'].T)
        grads['w2'] = np.dot(cache[6].T, d_h2)
        grads['b2'] = np.sum(d_h2, axis=0)
        # backprop drop1
        d_lrelu1 = dd_drop1 * dropout(cache[5], self.rate, derivative=True)
        # gradient on h1
        d_h1 = d_lrelu1 * lrelu(cache[4], self.alpha, derivative=True)
        # affine layer
        dd_drop0 = np.dot(d_h1, self.d_params['w1'].T)
        grads['w1'] = np.dot(cache[3].T, d_h1)
        grads['b1'] = np.sum(d_h1, axis=0)
        # backprop drop0
        d_lrelu0 = dd_drop0 * dropout(cache[2], self.rate, derivative=True)
        # gradient on h0
        d_h0 = d_lrelu0 * lrelu(cache[1], self.alpha, derivative=True)
        # affine layer
        d_x = np.dot(d_h0, self.d_params['w0'].T)
        grads['w0'] = np.dot(cache[0].T, d_h0)
        grads['b0'] = np.sum(d_h0, axis=0)
        return grads, d_x

    def _backprop_g(self, loss, cache, d_x):
        grads = {}
        d_tanh= 1/(loss + 10e-4) * d_x
        # gradient on D(G(x))
        d_h3 = d_tanh * tanh(cache[7], derivative=True)
        # affine layer       
        d_lrelu2 = np.dot(d_h3, self.g_params['w3'].T)
        grads['w3'] = np.dot(cache[6].T, d_h3)
        grads['b3'] = np.sum(d_h3, axis=0)
        # gradient on h2
        d_h2 = d_lrelu2 * lrelu(cache[5], self.alpha, derivative=True)
        # affine layer
        d_lrelu1 = np.dot(d_h2, self.g_params['w2'].T)
        grads['w2'] = np.dot(cache[4].T, d_h2)
        grads['b2'] = np.sum(d_h2, axis=0)
        # gradient on h1
        d_h1 = d_lrelu1 * lrelu(cache[3], self.alpha, derivative=True)
        # affine layer
        d_lrelu0 = np.dot(d_h1, self.g_params['w1'].T)
        grads['w1'] = np.dot(cache[2].T, d_h1)
        grads['b1'] = np.sum(d_h1, axis=0)
        # gradient on h0
        d_h0 = d_lrelu0 * lrelu(cache[1], self.alpha, derivative=True)
        # affine layer
        dd_z = np.dot(d_h0, self.g_params['w0'].T)
        grads['w0'] = np.dot(cache[0].T, d_h0)
        grads['b0'] = np.sum(d_h0, axis=0)
        return grads, dd_z

if __name__ == "__main__":
    from mnist import read
    
    DATA_PATH = './MNIST_data'
    training_data = list(read('training', DATA_PATH))
    train_images = np.array([x[1].reshape(1, -1) for x in training_data])
    train_images = train_images/255
    x = train_images.reshape(train_images.shape[0], train_images.shape[2])
    np.random.shuffle(x)

    gan = GAN(epochs=5)
    gan.train(x)
    # out, c = gan.discriminator(x[0:10])
    # print(out.shape)
    # print(out)