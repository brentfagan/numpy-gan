import numpy as np

def dropout(x, rate, derivative=False):
    mask = np.random.uniform(size=x.shape) > rate
    if not derivative:
        x *= mask * 1/(1-rate)
        return x
    else:
        return mask * 1/(1-rate)

def lrelu(x, alpha, derivative=False):
    if not derivative:
        return np.where(x < 0, alpha * x, x)
    else:
        return np.where(x < 0, alpha, 1)

def tanh(x, derivative=False):
    if not derivative:
        return np.tanh(x)
    else:
        return 1.0 - np.tanh(x) ** 2

def sigmoid(x, derivative=False):
    sig = 1/(1+np.exp(-x))
    if not derivative:
        return sig
    else:
        return sig * (1 - sig)

def weights_bias_init(in_dim, out_dim, dtype):
    w = xavier_init(in_dim, out_dim).astype(dtype)
    b = np.zeros(out_dim).astype(dtype)
    return w, b

def xavier_init(in_dim, out_dim):
    w = np.random.randn(in_dim, out_dim) * np.sqrt(2/(in_dim + out_dim))
    return w